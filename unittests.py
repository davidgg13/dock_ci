#!/usr/bin/env python3

import unittest
from vaja2 import bisect

class TestCases(unittest.TestCase):

    def test_case_1(self):
        with open("testni_primer1.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        self.assertTrue(bisect(rows)==2)
    
    def test_case_2(self):
        with open("testni_primer2.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        self.assertTrue(bisect(rows)==0)

    def test_case_3(self):
        with open("testni_primer3.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        self.assertTrue(bisect(rows)==999492)

    def test_case_4(self):
        with open("testni_primer4.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        self.assertTrue(bisect(rows)==999999579)


if __name__ == '__main__':
	unittest.main()
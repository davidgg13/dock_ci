#!/usr/bin/env python3
from vaja2 import bisect

def test_case_1():
        with open("testni_primer1.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        assert bisect(rows)==2

def test_case_2():
        with open("testni_primer2.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        assert bisect(rows)==0

def test_case_3():
        with open("testni_primer3.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        assert bisect(rows)==999492

def test_case_4():
        with open("testni_primer4.txt", "r") as file:
            n = int(file.readline())
            rows = [list(map(int, file.readline().split(' ')))]
            rows.append(list(map(int, file.readline().split(' '))))
        assert bisect(rows)==999999579
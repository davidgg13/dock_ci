#!/usr/bin/env python3
import sys

def checkPairs(rows):
    if len(rows[0])==1 or len(rows[1])==1:
        return False
    i=0
    while i < len(rows[0])-2:
        if rows[0][i]=='':
            i+=1
            continue
        if rows[0][i]==rows[0][i+1] and rows[0][i+1]!='':
            i+=2
        elif rows[0][i]==rows[0][i+2]:
            i+=3
        else:
            return False

    i=0
    while i < len(rows[1])-2:
        if rows[1][i]=='':
            i+=1
            continue
        if rows[1][i]==rows[1][i+1] and rows[1][i+1]!='':
            i+=2
        elif rows[1][i]==rows[1][i+2]:
            i+=3
        else:
            return False
    return True

def bisect(rows):
    if checkPairs(rows):
        return 0
    a=min(min(rows[0]),min(rows[1]))
    b=max(max(rows[0]),max(rows[1]))
    x=(a+b)//2
    f_a=[[i if i>a else '' for i in rows[0]],[i if i>a else '' for i in rows[1]]]
    f_x=[[i if i>x else '' for i in rows[0]],[i if i>x else '' for i in rows[1]]]
    while b!=a+1:#x-1!=a:#x-1!=a:

        if checkPairs(f_a)==False and checkPairs(f_x)==True:
            b=x
        else:
            a=x
            f_a=f_x
        x=(a+b)//2
        f_x=[[i if i>x else '' for i in rows[0]],[i if i>x else '' for i in rows[1]]]
    return x+1




t1=2
t2=0
t3=999492
t4=999999579